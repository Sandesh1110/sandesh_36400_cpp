/*
 * servicestation.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef SERVICESTATION_H_
#define SERVICESTATION_H_
#include "customer.h"
#include "bill.h"
#include "servicerequest.h"
class ServiceStation
{
private:
	list<Bill> billList;
	list<Customer> customerList;
	string name;
private:
	ServiceStation(const string& name)
	{
			this->name = name;
	}
	ServiceStation( const ServiceStation &other )
		{
			this->name = other.name;
		}
public:
	void setName( const string& name );
	string getName( void )const;
	list<Bill>& getBillList();
	list<Customer>& getCustomerList();
	void loadCustomer();
	void storeCustomer();
	void newCustomer(Customer c);
	void handleServiceRequest();
	void displayCustomerList();
    Customer* findCustomer(const string& name);
    void totalCollection();
	void storeBill();
	void loadBill();
	static ServiceStation& getInstance(const string& name ) //Factory method
	{
		static ServiceStation instance( name );
		return instance;
	}
};

#endif /* SERVICESTATION_H_ */
