#include "../include/bill.h"
    void Bill::setPaidAmount(const double paidAmount)
    {
    	this->paidAmount = paidAmount;
    }
    double Bill::getPaidAmount(void)
    {
    	return this->paidAmount;
    }
    double Bill::getAmount(void)
    {
    	return this->amount;
    }
    ServiceRequest* Bill::getServiceRequest()
    {
    	return request;
    }
    void Bill::computeAmount()
    {
    	list<Service*> :: iterator t;
		for(t= request->getServiceList().begin(); t != request->getServiceList().end(); ++t)
		{
			 (*t)->displayRecord();
			 amount = amount + (*t)->price();
		}
    }
    void Bill::computeTax()
    {
        double tax = (amount * 0.18);
        cout<<"||	tax Amount	:"<<tax<<endl;
    }
    void Bill::computeTotalBill()
    {
        amount = amount + (amount * 0.18);
        cout<<"||	Bill Amount	:"<<amount<<endl;
    }
    void Bill::displayRecord()
    {
    	this->computeAmount();
    	this->computeTax();
    	this->computeTotalBill();
    	cout<<"||======================================================================||"<<endl;
    	cout<<"Pay Bill		:	";
    	cin>>paidAmount;
    }

