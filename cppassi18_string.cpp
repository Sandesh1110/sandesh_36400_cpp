//============================================================================
// Name        : cppassi18_string.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <string>
using namespace std;
class String
{
private:
	size_t length;
	char *buff;
public:
	String(size_t len)
{
		cout<<"constructor"<<endl;
		this->length=len;
		buff = new char[length];
}
	void acceptRecord(char* arr)
	{
		memcpy(buff,arr,length);
	}

	void printRecord()
	{
		cout<<buff<<endl;
	}
	String operator+(String &other)
	{
		String temp(15);
		strcpy(temp.buff,this->buff);
		strcat(temp.buff,other.buff);
		return temp;
	}
	void stringReverse()
	{
		size_t j=length-2;
		for(size_t i=0; i<length; i++)
		{
				if(i<j)
				{
					char temp =this->buff[i];
					this->buff[i] = this->buff[j];
					this->buff[j] = temp;
					j--;
				}
		}
	}
    ~String()
    {
    	cout<<"destructor"<<endl;
    	delete[] buff;
    	buff=NULL;
    }
};
int main() {
   String s1(8),s2(8);
   s1.acceptRecord("Sandesh");
   s2.acceptRecord("Bhujbal");
   String s3 = s1 + s2; //s3 = s1.operator(s2);
   s3.printRecord();
   s1.stringReverse();
   s1.printRecord();
   s2.stringReverse();
   s2.printRecord();
   s3.stringReverse();
   s3.printRecord();
   return 0;
}


