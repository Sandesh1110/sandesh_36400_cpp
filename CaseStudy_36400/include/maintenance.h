/*
 * maintenance.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef MAINTENANCE_H_
#define MAINTENANCE_H_
#include <list>
#include "part.h"
#include "service.h"
class Maintenance : public Service
{
private:
	double laborCharges;
	list<Part> partList;
public:
	Maintenance() : laborCharges(0.0){}
	Maintenance(const double laborCharges);
	void setLaborCharges(const double laborCharges);
	double getLaborCharges();
	void addPart();
	list<Part>& getPartList();
    virtual void acceptRecord();
    virtual void displayRecord();
    virtual double price();
	virtual ~Maintenance(){}
};


#endif /* MAINTENANCE_H_ */
