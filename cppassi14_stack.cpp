//============================================================================
// Name        : cppassi14_stack.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Stack
{
private:
    int top;
    int *a; // Maximum size of Stack
public:
    Stack()
    {
    	top = -1;
    	a = NULL;
    	a = new int[10];
    }
    void push(int x)
    {
        top++;
        a[top]=x;
    }
    int peek()
    {
       return a[top];
    }
    void isEmpty()
    {
       if(top<0){
    	   cout<<"stack is empty";
       }
    }
    int pop()
	{
    	int b;
    	b=a[top];
    	top--;
    	return b;
    }
    ~Stack()
    {
    	delete[] a;
    	a = NULL;
    }
};

int main(void)
{
	Stack s1;
	int a,b;
	s1.push(50);
	a=s1.peek();
	cout<<a<<endl;
	s1.isEmpty();
	b=s1.pop();
	cout<<b<<endl;
	s1.isEmpty();
}
