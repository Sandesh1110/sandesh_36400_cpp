//============================================================================
// Name        : cppassi17_linklist.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Node
{
private:
	int data;
	Node* next;
public:
	Node() : data(0), next(NULL)
{}
	Node(int data)
	{
		this->data= data;
		this->next=NULL;
	}
	friend class Linklist;
};
class Linklist
{
private:
	Node* head;
	Node* tail;
public:
	Linklist() : head(NULL), tail(NULL)
{}
	bool empty( void )
	{
		return this->head == NULL;
	}
	void AddFirst(int data)
	{
		Node* newNode = new Node(data);
		if(this->empty())
		{
			this->head=newNode;
			this->tail=newNode;
		}
		else
		{
			Node *ptrNode = head;
			this->head=newNode;
			this->head->next = ptrNode;
		}
	}
	void AddAfter(int afterdata,int newdata)
	{
		Node* newNode = new Node(newdata);
		if(this->empty())
		{
			this->head=newNode;
			this->tail=newNode;
		}
		else
		{
			Node *ptr = this->head;
			while( ptr->next != NULL)
			{
				if(ptr->data == afterdata)
				{
					Node *ptrNode = ptr->next;
					ptr->next=newNode;
					newNode->next = ptrNode;
				}
				ptr = ptr->next;
			}
		}
	}
	void AddLast(int data)
	{
		Node* newNode = new Node(data);
		if(this->empty())
			this->head=newNode;
		else
			this->tail->next =newNode;
		this->tail=newNode;
	}
	void removeFirst()
	{
		if(this->empty())
			cout<<"linklist is Empty";
		else if(head==tail)
		{
			delete head;
			head=NULL;
			tail=NULL;
		}
		else
		{
			Node *ptrNode = head;
			head = head->next;
			delete ptrNode;
		}
	}
	void removeAny(int &data)
	{
		if(this->empty())
			cout<<"linklist is Empty";
		else if(head==tail)
		{
			delete head;
			head=NULL;
			tail=NULL;
		}
		else
		{
			Node *ptr = this->head;
			while( ptr->next != NULL)
			{
				if(ptr->next->data == data)
				{
					Node *ptrNode = ptr->next->next;
					delete ptr->next;
					ptr->next = ptrNode;
				}
				ptr = ptr->next;
			}
		}
	}
	void removeLast()
	{
		if(this->empty())
			cout<<"linklist is Empty";
		else if(head==tail)
		{
			delete head;
			head=NULL;
			tail=NULL;
		}
		else
		{
			Node *ptr = head;
			while( ptr->next->next != NULL)
			{
				ptr = ptr->next;
			}
			tail = ptr;
			delete ptr->next;
			ptr->next=NULL;
		}
	}
	void reverse()
	{
		Node* current = head;
		Node *prev = NULL, *next = NULL;
        while (current != NULL)
        {
			next = current->next;
			current->next = prev;
			prev = current;
			current = next;
		}
		head = prev;
	}
	void displayLinkList()
	{
		if(this->empty())
			cout<<"linklist is Empty";
		Node *ptr = this->head;
		while( ptr != NULL )
		{
			cout<<ptr->data<<endl;
			ptr = ptr->next;
		}
	}
	~Linklist( )
	{
		while( (empty( ) )!=0 )
			removeFirst();
	}
};

void acceptData(int &newdata)
{
	cout<<"Enter data	:"<<endl;
	cin>>newdata;
}

void acceptElement(int &newdata)
{
	cout<<"Enter data to delete		:"<<endl;
	cin>>newdata;
}
void acceptData(int &afterdata,int &newdata)
{
	cout<<"Enter element to add after:"<<endl;
	cin>>afterdata;
	cout<<"Enter data	:"<<endl;
	cin>>newdata;
}

int menu()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Add First"<<endl;
	cout<<"2.Add after element"<<endl;
	cout<<"3.Add Last"<<endl;
	cout<<"4.Remove First"<<endl;
	cout<<"5.Remove element"<<endl;
	cout<<"6.Remove Last"<<endl;
	cout<<"7.Reverse "<<endl;
	cout<<"8.Display Linked List"<<endl;
	cout<<"Enter choice"<<endl;
	cin>>choice;
	return choice;
}
int main() {
	Linklist A;
	int choice,newdata,afterdata;
	while((choice=menu())!=0)
	{
		switch(choice)
		{
		case 1:
			::acceptData(newdata);
			A.AddFirst(newdata);
			break;
		case 2:
			::acceptData(afterdata,newdata);
			A.AddAfter(afterdata,newdata);
			break;
		case 3:
			::acceptData(newdata);
			A.AddLast(newdata);
			break;
		case 4:
			A.removeFirst();
			break;
		case 5:
			::acceptElement(newdata);
			A.removeAny(newdata);
			break;
		case 6:
			A.removeLast();
			break;
		case 7:
			A.reverse();
			break;
		case 8:
			A.displayLinkList();
			break;
		}
	}
	return 0;
}
