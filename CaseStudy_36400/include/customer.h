/*
 * customer.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
#include <iostream>
#include <vector>
#include <list>
#include <string>
#include "vehicle.h"
using namespace std;
class Customer
{
private:
	string name;
	string mobile;
	string address;
	vector<Vehicle> vehicleList;
public:
	Customer() : name(""), mobile(""), address("") {}
	Customer(const string& name,const string& mobile,const string& address);
	void setAddress(const string& address);
	void setMobile(const string& mobile);
	void setName(const string& name);
	void setVehicle(const Vehicle v);
	string& getAddress(void);
	string& getMoblie(void);
	string& getName(void);
	vector<Vehicle>& getVehicle(void);
    Vehicle& newVehicle();
	void displayVehicles();
	friend ostream& operator<<(ostream &cout, const Customer& other);
	friend istream& operator>>(istream &cin, Customer& other);
};

#endif /* CUSTOMER_H_ */
