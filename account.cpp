/*
 * account.cpp
 *
 *  Created on: 28-Jul-2020
 *      Author: sunbeam
 */
#include <iostream>
using namespace std;
#include"../include/account.h"
int count=1000;
void Account::setAccno()
{
	accno=++count;
}
void Account::setName()
{
	cout<<"Name		:";
	cin>>name;
}
void Account::setType()
{
	cout<<"Type		:";
	cin>>type;
}
void Account::setBalance()
{
	cout<<"Balance	:";
	cin>>balance;
}
int Account::getAccno()
{
	return accno;
}
string Account::getName()
{
	return name;
}
string Account::getType()
{
	return type;
}
float Account::getBalance()
{
	return balance;
}
