/*
 * service.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef SERVICE_H_
#define SERVICE_H_
#include <iostream>
#include <string>
using namespace std;
class Service
{
private:
	string desc;
public:
	Service(): desc(""){}
	Service(const string& desc)
		{
			setDesc(desc);
		}
	void setDesc(const string& desc);
	string getDesc()const;
	virtual void acceptRecord();
	virtual void displayRecord();
	virtual double price()=0;
	virtual ~Service(){}
};

#endif /* SERVICE_H_ */
