//============================================================================
// Name        : cppassi14_matrix.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
using namespace std;
class Matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	Matrix()
{
		this->row=0;
		this->col=0;
		this->arr=NULL;
}
	Matrix(int row, int col)
	{
		this->row=row;
		this->col=col;
		arr = new int*[row];
		for(int i=0; i<row; i++)
		{
			arr[i]=new int[col];
		}
	}
	void acceptRecord( )
	{
		int ele;
		for(int i=0; i<row; i++)
		{
			for(int j=0; j<col; j++)
			{
				cout<<"Element:";
				cin>>ele;
				this->arr[i][j]=ele;
			}
		}
	}
	void printRecord()
	{
		for(int i=0; i<row; i++)
		{
			for(int j=0; j<col; j++)
			{
				cout<<arr[i][j]<<" ";
			}
			cout<<endl;
		}
	}
	~Matrix()
	{
		for( int index = 0; index < row; ++ index )
			delete[] arr[ index ];
		delete[] arr;
		arr = NULL;
	}

};
int main() {
	Matrix M(3,3);
	M.acceptRecord();
	M.printRecord();
	return 0;
}
