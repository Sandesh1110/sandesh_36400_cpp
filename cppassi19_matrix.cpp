//============================================================================
// Name        : cppassi19_matrix.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
using namespace std;
class Matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	Matrix() : row(0), col(0), arr(NULL)
{}
	Matrix(int row, int col)
	{
		this->row = row;
		this->col = col;
		arr = new int*[row];
		for(int i=0; i<row; i++)
		{
			arr[i]=new int[col];
		}
	}
	Matrix operator+(Matrix &other)
	{
		Matrix temp(row,col);
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<row;j++)
			{
				temp.arr[i][j]=this->arr[i][j]+other.arr[i][j];
			}
		}
		return temp;
	}
	Matrix operator-(Matrix &other)
	{
		Matrix temp(row,col);
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<row;j++)
			{
				temp.arr[i][j]=this->arr[i][j]-other.arr[i][j];
			}
		}
		return temp;
	}
	Matrix operator*(Matrix &other)
	{
		Matrix temp(this->row,other.col);
		for(int i=0;i<this->row;i++)
		{
			for(int j=0;j<other.col;j++)
			{
				for(int k=0; k<this->col; k++)
				{
					temp.arr[i][j]+=((this->arr[i][k]) * (other.arr[k][j]));
				}
			}
		}
		return temp;
	}
	Matrix operator=(const Matrix &other)
	{
		this->~Matrix();
		this->row = other.row;
		this->col = other.col;
		this->arr = new int*[other.row];
		for(int i=0; i<other.row; i++)
		{
			this->arr[i] = new int[other.col];
		}
		for(int i=0; i<this->row; i++)
		{
			for(int j=0; j<this->col; j++)
			{
				this->arr[i][j] = other.arr[i][j];
			}
		}
		return *this;
	}
	Matrix (const Matrix &other)
	{
		this->row = other.row;
		this->col = other.col;
		this->arr = new int*[other.row];
		for(int i=0; i<other.row; i++)
		{
			this->arr[i] = new int[other.col];
		}
		for(int i=0; i<this->row; i++)
		{
			for(int j=0; j<this->col; j++)
			{
				this->arr[i][j] = other.arr[i][j];
			}
		}
	}
	friend istream& operator>>(istream &cin, Matrix& other)
	{
		for (int i = 0; i < other.row ; i++)
		{
			for (int j = 0; j < other.col ; j++)
			{
				cout << "enter element";
				cin>>other.arr[i][j];
			}
		}
		return cin;
	}
	friend ostream& operator<<(ostream& cout, Matrix& other)
	{
		for (int i = 0; i < other.row; i++)
		{
			for (int j = 0; j < other.col; j++)
			{
				cout << other.arr[i][j];
				cout<<" ";
			}
			cout << endl;
		}
		return cout;
	}
	~Matrix()
	{
		if(this->arr != NULL)
		{
			for( int index = 0; index < row; ++ index )
				delete[] arr[ index ];
			delete[] arr;
			arr = NULL;
		}
	}

};


int menu()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Matrix Addition"<<endl;
	cout<<"2.Matrix Subtraction"<<endl;
	cout<<"3.Matrix Multiplication"<<endl;
	cout<<"Enter choice"<<endl;
	cin>>choice;
	return choice;
}
int main() {
	Matrix M1(2,2);
	cin>>M1;
	Matrix M2(2,2);
	cin>>M2;
	Matrix M3(2,2);
	Matrix M4(2,2);
	Matrix M5(2,2);
	int choice;
	while((choice=menu())!=0)
	{
		switch(choice)
		{
		case 1:
			M3= M1+(M2);
			cout<<M3;
		break;
		case 2:
			M4= M1 -(M2);
			cout<<M4;
		break;
		case 3:
			M5= M1 * M2;
			cout<<M5;
		break;
		}
	}
	return 0;
}
