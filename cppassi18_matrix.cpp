//============================================================================
// Name        : cppassi18_matrix.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
using namespace std;
class Matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	Matrix() : row(0), col(0), arr(NULL)
{}
	Matrix(int row, int col)
	{
		setRow(row);
		setCol(col);
		arr = new int*[row];
		for(int i=0; i<row; i++)
		{
			arr[i]=new int[col];
		}
	}
	void setRow(const int row)
	{
		this->row=row;
	}
	void setCol(const int col)
	{
		this->col=col;
	}
	int getRow()
	{
		return this->row;
	}
	int getCol()
	{
		return this->col;
	}
	void setElement( const int row, const int col, const int element )
	{
		this->arr[row][col]=element;
	}
	int getElement( int row ,int col )const
	{
		return this->arr[row][col];
	}
	Matrix operator+(Matrix &other)
	{
		Matrix temp(row,col);
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<row;j++)
			{
				temp.arr[i][j]=this->arr[i][j]+other.arr[i][j];
			}
		}
		return temp;
	}
	Matrix operator-(Matrix &other)
	{
		Matrix temp(row,col);
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<row;j++)
			{
				temp.arr[i][j]=this->arr[i][j]-other.arr[i][j];
			}
		}
		return temp;
	}
	Matrix operator*(Matrix &other)
	{
		Matrix temp(this->row,other.col);
		for(int i=0;i<this->row;i++)
		{
			for(int j=0;j<other.col;j++)
			{
				for(int k=0; k<this->col; k++)
				{
					temp.arr[i][j]+=((this->arr[i][k]) * (other.arr[k][j]));
				}
			}
		}
		return temp;
	}
    Matrix (const Matrix &other)
    {
    	this->row = other.row;
    	this->col = other.col;
    	this->arr = new int*[other.row];
    	for(int i=0; i<other.row; i++)
    	{
    		this->arr[i] = new int[other.col];
    	}
    	for(int i=0; i<this->row; i++)
    	{
    		for(int j=0; j<this->col; j++)
    		{
    			this->arr[i][j] = other.arr[i][j];
    		}
    	}
    }
	~Matrix()
	{
		for( int index = 0; index < row; ++ index )
			delete[] arr[ index ];
		delete[] arr;
		arr = NULL;
	}

};

void acceptRecord(Matrix &M)
{
	int ele;
	for(int i=0; i<M.getRow(); i++)
	{
		for(int j=0; j<M.getCol(); j++)
		{
			cout<<"Element:";
			cin>>ele;
			M.setElement(i,j,ele);
		}
	}
}

void printRecord(Matrix &M)
{
	for(int i=0; i<M.getRow(); i++)
	{
		for(int j=0; j<M.getCol(); j++)
		{
			cout<<M.getElement(i,j)<<" ";
		}
		cout<<endl;
	}
}
 int menu()
 {
	 int choice;
	 cout<<"0.Exit"<<endl;
	 cout<<"1.Matrix Addition"<<endl;
	 cout<<"2.Matrix Subtraction"<<endl;
	 cout<<"3.Matrix Multiplication"<<endl;
	 cout<<"Enter choice"<<endl;
	 cin>>choice;
	 return choice;
 }
int main() {
	Matrix M1(2,2);
	acceptRecord(M1);
	Matrix M2(2,2);
	acceptRecord(M2);
	int choice;
    while((choice=menu())!=0)
    {
    	switch(choice)
    	{
    	case 1:
    	{
    		Matrix M3= M1.operator +(M2);
    		printRecord(M3);
    	}
    	break;
    	case 2:
    	{
    		Matrix M3= M1.operator -(M2);
    		printRecord(M3);
    	}
    	break;
    	case 3:
    	{
    		Matrix M3= M1.operator *(M2);
    		printRecord(M3);
    	}
    	break;
    	}
    }
	return 0;
}
