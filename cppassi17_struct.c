/*
 ============================================================================
 Name        : cppassi17_struct.c
 Author      : sandesh
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
struct Complex
{
	int real;
	int imag;
};
struct Complex addition(struct Complex* a,struct Complex* b)
{
	struct Complex result;
	result.real=a->real + b->real;
	result.imag=a->imag + b->imag;
	return result;
}
int main(void) {
	struct Complex arr,arr2,result;
    arr.real=10;
    arr.imag=20;
    arr2.real=30;
    arr2.imag=40;
    result = addition(&arr,&arr2);
    printf("real	:%d\n",result.real);
    printf("imag	:%d\n",result.imag);
	return 0;
}
