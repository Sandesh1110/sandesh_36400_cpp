#include"../include/part.h"
Part::Part(const string& desc,const double rate)
	{
		setDesc(desc);
		setRate(rate);
	}
	void Part::setDesc(const string& desc)
	{
		for(unsigned i=0; i<desc.length(); i++)
			if(isdigit(desc[i]))
				throw("Description Name Should be in alphabets");
		this->desc = desc;
	}
	void Part::setRate(const double rate)
	{
		this->rate = rate;
	}
	string& Part::getDesc()
	{
		return this->desc;
	}
	double Part::getRate()
	{
		return this->rate;
	}
	void Part::acceptRecord()
	{
		string desc;
		double rate;
		cout<<"Enter DESC	:	";
		cin>>desc;
		setDesc(desc);
		cout<<"Enter Rate	:	";
		cin>>rate;
		setRate(rate);
	}
	void Part::displayRecord()
	{
		cout<<"||	Part Name:  "<<this->getDesc()<<"	Rate:  "<<this->getRate()<<endl;
	}

