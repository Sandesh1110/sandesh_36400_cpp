//============================================================================
// Name        : cppassi20_array.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
template<class T>
class Array
{
private:
	int size;
	T *arr;
public:
	 Array(int size = 0)
{
		this->size = size;
		this->arr = NULL;
		if(this->size != 0)
			this->arr = new T(this->size);
}
	friend istream& operator>>(istream &cin, Array &other)
	{
		cout<<"Enter array	:";
		for(int i=0; i<other.size; i++)
		{
			cin>>other.arr[i];
		}
		return cin;
	}
	friend ostream& operator<<(ostream &cout,const Array &other)
	{
		for(int i=0; i<other.size; i++)
		{
			cout<<other.arr[i];
		}
		return cout;
	}
	operator int()
    {
		return this->size;
	}
	Array& operator=(Array& other)
	{
		this->~Array();
		this->size = other.size;
		this->arr = new T(this->size);
		for(int i=0; i<this->size; i++)
		{
			this->arr[i] = other.arr[i];
		}
	}
	~Array()
	{
		delete this->arr;
		this->arr = NULL;
	}
};
int main() {
	Array<int> A(1);
    cin>>A;
    cout<<A;
    cout<<endl;
    Array<char> A1(7);
    cin>>A1;
    cout<<A1;
    cout<<endl;
    Array<float> A2(4);
    cin>>A2;
    cout<<A2;
	return 0;
}
