/*
 * vehicle.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef VEHICLE_H_
#define VEHICLE_H_
#include <iostream>
#include <vector>
#include <string>
using namespace std;
class Vehicle
{
private:
	string company;
	string model;
	string lnumber;
	string mobile;
public:
	Vehicle() :  company("x"), model("x") ,lnumber("x"),mobile("x") {}
	Vehicle(const string& company,const string& model,const string& lnumber,const string& mobile);
	void setCompany(const string& company);
	void setModel(const string& model);
	void setLNumber(const string& lnumber);
	void setMobile(const string& mobile);
	string& getCompany(void);
	string& getModel(void);
	string& getLNumber(void);
	string& getMoblie(void);
	void acceptRecord();
	void display();
	friend ostream& operator<<(ostream &cout, const Vehicle& other);
	friend istream& operator>>(istream &cin, Vehicle& other);
	Vehicle operator=(const Vehicle &other);
};


#endif /* VEHICLE_H_ */
