/*
 * bill.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef BILL_H_
#define BILL_H_
#include <iostream>
#include "servicerequest.h"
#include "service.h"
class Bill
{
private:
	double amount;
	double paidAmount;
	ServiceRequest* request;
public:
	Bill(const double amount=0.0,const double paidAmount=0.0,ServiceRequest* request=NULL) : amount(amount), paidAmount(paidAmount)
    {
	      this->request = request;
    }
	Bill(ServiceRequest* request) : amount(0.0), paidAmount(0.0)
    {
	      this->request = request;
    }
    void setPaidAmount(const double paidAmount);
    double getPaidAmount(void);
    double getAmount(void);
    ServiceRequest* getServiceRequest();
    void computeAmount();
    void computeTax();
    void computeTotalBill();
    void displayRecord();
};

#endif /* BILL_H_ */
