//============================================================================
// Name        : cppassi19_string.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <string>
using namespace std;
class String
{
private:
	size_t length;
	char *buff;
public:
	String(void) : length(0), buff(NULL)
{}
	String(const char* str)
	{
		this->length = strlen(str);
		this->buff = new char[this->length+1];
		strcpy(this->buff,str);
	}
	String(size_t len)
	{
		this->length=len;
		this->buff = new char[this->length+1];
	}
	String (const String &other)
	{
		this->length = other.length;
		this->buff = new char[this->length+1];
		strcpy(this->buff,other.buff);
	}
	String operator+(String &other)
	{
		String temp(this->length+other.length);
		strcpy(temp.buff,this->buff);
		strcat(temp.buff,other.buff);
		return temp;
	}
	String& operator=( const String &other)
	{
		this->~String();
		this->length = other.length;
		this->buff = new char[this->length+1];
		strcpy(this->buff,other.buff);
		return *this;
	}
/*	void operator()(const char* str)
	{

	}
	void operator()(const char* str)
	{

	}*/
	void upperCase()
	{
		for(size_t i=0; i<this->length; i++)
		{
			if(islower(this->buff[i]))
			{
				this->buff[i] = this->buff[i]-32;
			}
		}
	}
	void lowerCase()
	{
		for(size_t i=0; i<this->length; i++)
		{
			if(isupper(this->buff[i]))
			{
				this->buff[i] = this->buff[i]+32;
			}
		}
	}
	friend istream& operator>>(istream &cin, String &other)
	{
		cout<<"extractin ";
		cout<<"Enter string		:";
		cin>>other.buff;
		return cin;
	}
	friend ostream& operator<<(ostream &cout, const String &other)
	{
		cout<<"insertion ";
		cout<<other.buff<<endl;
		return cout;
	}
	friend void operator~(String &s)
	{
		size_t j=s.length-1;
		for(size_t i=0; i<s.length; i++)
		{
			if(i<j)
			{
				char temp =s.buff[i];
				s.buff[i] = s.buff[j];
				s.buff[j] = temp;
				j--;
			}
		}
    }
	~String()
	{
		if(this->buff !=NULL)
		{
			delete[] this->buff;
			this->buff=NULL;
		}
	}
};


int main() {
	String s1(8),s2(8);
	cin>>s1;
	cout<<s1;
	cin>>s2;
	cout<<s2;
	String s3;
	s3 = s1 + s2;
	cout<<s3<<endl;
	String s4;

	return 0;
}
