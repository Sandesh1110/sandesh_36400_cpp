//============================================================================
// Name        : cppassiday12.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
int count=1000;
class Account
{
private:
	int accid;
	string name;
	string type;
	float balance;
public:
	Account(int accid = 0, string name = "", string type = "", float balance = 0.0) throw() : accid (accid), name (name), type (type), balance (balance)
{}
	void setAccid(const int &accid)
	{
		this->accid = accid;
	}
	void setName(const string &name)
	{
		this->name = name;
	}
	void setType(const string &type)
	{
		this->type = type;
	}
	void setBalance(const float &balance)
	{
		this->balance = balance;
	}
	int getAccid()const
	{
		return this->accid;
	}
	string getName()const
	{
		return this->name;
	}
	string getType()const
	{
		return this->type;
	}
	float getBalance()const
	{
		return this->balance;
	}
};

class Bank
{
private:
	int index=-1;
	Account arr[5];
public:
	void createAccount()
	{
		++index;
		arr[index].setAccid(++count);
		string name;
		cout<<"Name		:";
		cin>>name;
		arr[index].setName(name);
		string type;
		cout<<"Type		:";
		cin>>type;
		arr[index].setType(type);
		float balance;
		cout<<"Balance		:";
		cin>>balance;
		arr[index].setBalance(balance);
	}
	void displayAccount(const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if(accid==(arr[i].getAccid()))
			{
				cout<<"Account number	:"<<arr[i].getAccid()<<endl;
				cout<<"Name				:"<<arr[i].getName()<<endl;
				cout<<"Type				:"<<arr[i].getType()<<endl;
				cout<<"Balance			:"<<arr[i].getBalance()<<endl;
			}

		}
	}
	void amountDeposit(const float &amount,const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if(accid==(arr[i].getAccid()))
			{
				float am;
				am=arr[i].getBalance()+amount;
				arr[i].setBalance(am);
			}
		}
	}
	void amountWithdraw(const float &amount, const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if(arr[i].getBalance()<amount)
				throw "Insufficient Balance";
			if(accid==arr[i].getAccid())
			{
				float am;
				am=arr[i].getBalance()-amount;
				arr[i].setBalance(am);
			}
		}
	}
};

int menu()
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Create Account."<<endl;
	cout<<"2.Display Account."<<endl;
	cout<<"3.Deposit Amount."<<endl;
	cout<<"4.Withdraw Amount."<<endl;
	cout<<"Enter Choice."<<endl;
	cin>>choice;
	return choice;
}

int getAccountNumber()
{
	int accno;
	cout<<"Enter Account Number		:";
	cin>>accno;
	return accno;
}

int getAmount()
{
	int amount;
	cout<<"Enter Amount		:";
	cin>>amount;
	return amount;
}

int main() {
	Bank b;
	int acc,amount,choice;
	while((choice=menu())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				b.createAccount();
				break;
			case 2:
				acc=getAccountNumber();
				b.displayAccount(acc);
				break;
			case 3:
				acc=getAccountNumber();
				amount=getAmount();
				b.amountDeposit(amount,acc);
				break;
			case 4:
				acc=getAccountNumber();
				amount=getAmount();
				b.amountWithdraw(amount,acc);
				break;
			}
		}
		catch(char const* messege)
		{
			cout<<messege<<endl;
		}
		catch(...)
		{
			cout<<"terminated with exeption value"<<endl;
		}
	}
	return 0;
}







