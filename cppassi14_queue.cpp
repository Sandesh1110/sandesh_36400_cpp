//============================================================================
// Name        : cppassi14_queue.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Queue
{
private:
	int length;
	int *arr;
	int front;
	int rear;
public:
	Queue(void)
{
		this->length=0;
		this->front=0;
		this->rear=-1;
		this->arr=NULL;
}
	Queue(int len)
	{
		this->length=len;
		this->front=0;
		this->rear=-1;
		this->arr=NULL;
		arr = new int[length];
	}
	bool is_queue_full(void)
	{
		if(rear == (length- 1))
			return true;
		else
			return false;
	}
	bool is_queue_empty(void)
	{
		if(front < 0 || front > rear)
			return true;
		else
			return false;
	}
	void enqueue(int ele)
	{
		if(is_queue_full())
			cout<<"NO element inserted Queue is Full"<<endl;
		else
		{
			rear = rear + 1;
			arr[rear] = ele;
			if(is_queue_full())
				cout<<"Queue is Full"<<endl;
		}
	}
	void dequeue(void)
	{
		if(is_queue_empty())
			cout<<"Queue is Empty"<<endl;
		if(front==rear)
		{
			front=0;
			rear=-1;
			cout<<"Queue is Empty"<<endl;
		}
		else
			front = front + 1;
	}
	int get_front(void)
	{
		return arr[front];
	}
	~Queue()
	{
		delete[] arr;
		arr = NULL;
	}
};

int menu()
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Check Queue Is Full or not."<<endl;
	cout<<"2.Check Queue Is Empty or not."<<endl;
	cout<<"3.Enqueue."<<endl;
	cout<<"4.Dequeue."<<endl;
	cout<<"5.Get Front."<<endl;
	cout<<"Enter Choice."<<endl;
	cin>>choice;
	return choice;
}

int main()
{
	Queue q(5);
	int choice,b;
	bool a;
	while((choice=menu())!=0)
	{
		switch(choice)
		{
		case 1:
			a=q.is_queue_full();
			cout<<a<<endl;
			if(a==1)
				cout<<"queque is full"<<endl;
			else
				cout<<"queque is not full"<<endl;
			break;
		case 2:
			a=q.is_queue_empty();
			if(a==1)
				cout<<"queque is empty"<<endl;
			else
				cout<<"queque is not empty"<<endl;
			break;
		case 3:
			cout<<"Enter element	:";
			cin>>b;
			q.enqueue(b);
			break;
		case 4:
			q.dequeue();
			break;
		case 5:
			b=q.get_front();
			cout<<"front is 	:"<<b<<endl;
			break;
		}
	}
	return 0;
}
