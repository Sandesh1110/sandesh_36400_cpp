#include <fstream>
#include <string>
#include <sstream>
#include "../include/servicestation.h"


void ServiceStation::setName( const string& name )
{
	this->name = name;
}
string ServiceStation::getName( void )const
{
	return this->name;
}
list<Bill>& ServiceStation::getBillList()
{
	return this->billList;
}
list<Customer>& ServiceStation::getCustomerList()
{
	return this->customerList;
}
void ServiceStation::loadCustomer()
{
	ifstream fOpenCus, fOpenVeh;
	string line;
	int i=0;
	fOpenCus.open("Customer.csv");
	if(!fOpenCus)
	{
		perror("failed to open Customer file");
		return;
	}
	while(getline(fOpenCus, line)) {
		stringstream str(line);
		string name;
		string mobile;
		string address;
		getline(str, name, ',');
		getline(str, mobile, ',');
		getline(str, address);
		Customer c(name, mobile, address);
		fOpenVeh.open("Vehicle.csv");
		if(!fOpenVeh)
		{
			perror("failed to open Vehicle file");
			return;
		}
		while(getline(fOpenVeh, line)) {
			stringstream str(line);
			string company;
			string model;
			string lnumber;
			string mob;
			getline(str, company, ',');
			getline(str, model, ',');
			getline(str, lnumber, ',');
			getline(str, mob);
			Vehicle v(company, model,lnumber,mob);
			if(c.getMoblie()==mob)
			{
				c.setVehicle(v);
			}
		}
		fOpenVeh.close();
		customerList.push_back(c);
		i++;
	}
	fOpenCus.close();
}
void ServiceStation::storeCustomer()
{
	ofstream fOpenCus, fOpenVeh;
	unsigned int  j=0;
	fOpenCus.open("Customer.csv");
	fOpenVeh.open("Vehicle.csv");
	if(!fOpenCus)
	{
		cout<<"failed to open Customer file";
		return;
	}
	if(!fOpenVeh)
	{
		cout<<"failed to open Vehicle file";
		return;
	}
	list<Customer> :: iterator t;
	for(t = customerList.begin(); t != customerList.end(); ++t)
	{
		fOpenCus<<t->getName()<< ","<<t->getMoblie()<<","
				<< t->getAddress()<<endl;

		vector<Vehicle> temp = t->getVehicle();
		for( j=0;j<temp.size(); j++)
		{
			fOpenVeh<<temp[j].getCompany()<<","<<temp[j].getModel()<<","<<temp[j].getLNumber()<<","
					<<t->getMoblie()<<endl;
		}

	}
	fOpenVeh.close();
	fOpenCus.close();
}
void ServiceStation::newCustomer(Customer c)
{
	customerList.push_back(c);
}
void ServiceStation::handleServiceRequest()
{
	string name;
	string lname;
	string mobile;
	int select;
	cout<<"Enter Name Of Customer	:	";
	cin>>name;
	for(unsigned i=0; i<name.length(); i++)
	{
		if(isdigit(name[i]))
			throw("Name Should be in alphabets");
	}
	Customer *f = this->findCustomer(name);
	Vehicle t;
	do
	{
		f->displayVehicles();
		cout<<"0.Exit."<<endl<<"1.Add Vehicle."<<endl<<"Select Option	:	";
		cin>>select;
		switch(select)
		{
		case 1:
			t.acceptRecord();
			mobile = f->getMoblie();
			t.setMobile(mobile);
            f->setVehicle(t);
			break;
		}
	}while(select!=0);
	cout<<"Enter Vehicle number:";
	cin>>lname;
	for(unsigned i=0; i<lname.length(); i++)
	{
		if(isalpha(lname[i]))
			throw("Vehicle number Should be in numeric");
	}
	ServiceRequest s(name,lname);
	s.serviceProcess();
	Bill b(&s);
	cout<<"||======================================================================||"<<endl;
	cout<<"||				Morris_Garages				||"<<endl;
	cout<<"||======================================================================||"<<endl;
	b.displayRecord();
	billList.push_back(b);
}
void ServiceStation::displayCustomerList()
{
	int i=0;
	list<Customer> :: iterator t;
	cout<<endl;
	cout<<"||======================================================================||"<<endl;
	for(t = customerList.begin(); t != customerList.end(); ++t)
	{
		i++;
		cout<<"Customer ("<<i<<"). "<<*t<<endl;
	}
	cout<<"||======================================================================||"<<endl<<endl;
}
Customer* ServiceStation::findCustomer(const string& name)
{
	Customer *temp=NULL;
	list<Customer> :: iterator t;
	for(t = customerList.begin(); t != customerList.end(); ++t)
	{
		if(t->getName() == name)
		{
			temp = &(*t);
			return temp;
		}
	}
	throw("Customer Not Found");
	return temp;
}
void ServiceStation::totalCollection()
{
	double totalCollection;
	double bills;
	int index=100;
	list<Bill> :: iterator b;
	for(b = getBillList().begin(); b != getBillList().end(); ++b)
	{
		index++;
		cout<<"||		Bill Amount			Paid Amount		||"<<endl;
		cout<<"||----------------------------------------------------------------------||"<<endl;
		cout<<"||Bill No."<<index<<"	"<<b->getAmount()<<"				"<<b->getPaidAmount()<<"			||"<<endl;
		bills = bills + b->getAmount();
		totalCollection = totalCollection + b->getPaidAmount();
	}
	cout<<"||----------------------------------------------------------------------||"<<endl;
	cout<<"||		Total Bill Amount:"<<bills<<"	Total Business:	"<<totalCollection<<"	||"<<endl;
}
void ServiceStation::storeBill()
{
	ofstream fOpenBill;
	fOpenBill.open("Bill.csv");
	if(!fOpenBill)
	{
		cout<<"failed to open Bill file";
		return;
	}
	list<Bill> :: iterator b;
	for(b = getBillList().begin(); b != getBillList().end(); ++b)
	{

		fOpenBill<<b->getAmount()<< ","<<b->getPaidAmount()<<","
				<<"NULL"<<endl;
	}
	fOpenBill.close();
}
void ServiceStation::loadBill()
{
	ifstream fOpenBill;
	string line;
	fOpenBill.open("Bill.csv");
	if(!fOpenBill)
	{
		perror("failed to open Customer file");
		return;
	}
	while(getline(fOpenBill, line)) {
		stringstream str(line);
		double amount;
		double payedAmount;
		string name;
		string lnumber;
		char ch;
		str >> amount >> ch;
		str >> payedAmount >> ch;
		getline(str, name, ',');
		getline(str, lnumber);
		Bill b(amount,payedAmount,NULL);
		billList.push_back(b);
	}
	fOpenBill.close();
}


