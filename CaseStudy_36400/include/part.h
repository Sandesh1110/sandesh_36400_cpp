/*
 * part.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef PART_H_
#define PART_H_
#include <iostream>
#include <string>
using namespace std;
class Part
{
private:
	string desc;
	double rate;
public:
	Part(void) : desc(" ") , rate( 0.0){}
	Part(const string& desc,const double rate);
	void setDesc(const string& desc);
	void setRate(const double rate);
	string& getDesc();
	double getRate();
	void acceptRecord();
	void displayRecord();
};

#endif /* PART_H_ */
