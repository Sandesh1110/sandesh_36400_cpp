#include "../include/servicerequest.h"
ServiceRequest::ServiceRequest(const string& customerName,const string& vehicleNumber)
{
	setCustomerName(customerName);
	setVehicleNumber(vehicleNumber);
}
void ServiceRequest::setCustomerName(const string& customerName)
{
	this->customerName = customerName;
}
void ServiceRequest::setVehicleNumber(const string& vehicleNumber)
{
	this->vehicleNumber = vehicleNumber;
}
string& ServiceRequest::getCustomerName(void)
{
	return this->customerName;
}
string& ServiceRequest::getVehicleNumber(void)
{
	return this->vehicleNumber;
}
list<Service*>& ServiceRequest::getServiceList()
{
	return serviceList;
}
void ServiceRequest::serviceProcess()
{
	int ch;
	do{
		cout<<"0.Exit."<<endl<<"1.Oil Change."<<endl<<"2.Maintenance/Repairing."<<endl<<"Enter Choice."<<endl;
		cin>>ch;
		Service *temp=NULL;
		try
		{
			switch (ch)
			{
			case 1:
				temp = new Oil();
				temp->acceptRecord();
				serviceList.push_back(temp);
				break;
			case 2:
				temp = new Maintenance();
				temp->acceptRecord();
				int c;
				do
				{
					cout<<"0.Exit."<<endl<<"1.Add Part."<<endl;
					Maintenance *m = (Maintenance*)temp;
					cin>>c;
					switch(c)
					{
					case 1:
						m->addPart();
						break;
					}
				}while(c!=0);
				serviceList.push_back(temp);
				break;
			}
		}
		catch(char const* b)
		{
			cout<<b<<endl;
		}
	}while(ch!=0);
}

