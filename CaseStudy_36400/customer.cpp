#include "../include/customer.h"
#include <cctype>
Customer::Customer(const string& name,const string& mobile,const string& address)
{
	setAddress(address);
	setMobile(mobile);
	setName(name);
}
void Customer::setAddress(const string& address)
{
	this->address = address;
}
void Customer::setMobile(const string& mobile)
{
	this->mobile = mobile;
}
void Customer::setName(const string& name)
{
	this->name = name;
}
void Customer::setVehicle(const Vehicle v)
{
	this->vehicleList.push_back(v);
}
string& Customer::getAddress(void)
{
	return this->address;
}
string& Customer::getMoblie(void)
{
	return this->mobile;
}
string& Customer::getName(void)
{
	return this->name;
}
vector<Vehicle>& Customer::getVehicle(void)
{
	return this->vehicleList;
}
Vehicle& Customer::newVehicle()
{
	Vehicle v;
	cin>>v;
	return v;
}
void Customer::displayVehicles()
{
	for(unsigned i=0; i<vehicleList.size(); i++)
	{
		cout<<vehicleList[i]<<endl;
	}
}
ostream& operator<<(ostream &cout, const Customer& other)
{
	cout<<other.name<<","<<other.mobile<<", "<<other.address<<endl;
	for(unsigned i=0; i<other.vehicleList.size(); i++)
	{
		cout<<"\t\t"<<(i+1)<<". "<<other.vehicleList[i];
	}

	return cout;
}
istream& operator>>(istream &cin, Customer& other)
{
	cout<<"Name	:	";
	cin>>other.name;
	for(unsigned i=0; i<other.name.length(); i++)
	{
		if(isdigit(other.name[i]))
			throw("Name Should be in alphabets");
	}
	cout<<"Mobile	:	";
	cin>>other.mobile;
	for(unsigned i=0; i<other.mobile.length(); i++)
	{
		if(isalpha(other.mobile[i]))
			throw("Mobile number Should be in numeric");
	}
	cout<<"Address	:	";
	cin>>other.address;
	int ch;
	do{
		cout<<"0.Exit."<<endl<<"1.New Vehicle."<<endl<<"Enter choice."<<endl;
		cin>>ch;
		Vehicle v;
		switch(ch)
		{
		case 1:
		v.setMobile(other.mobile);
		cin>>v;
		other.vehicleList.push_back(v);
		break;
		}
	  }while(ch!=0);
	return cin;
}

