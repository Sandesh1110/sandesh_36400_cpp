//============================================================================
// Name        : 2.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Date
{
	int day;
	int month;
	int year;
public:
	void initDate()
	{
		day=1;
		month=1;
		year=2020;
	}
	void acceptRecord()
	{
		cout<<"Enter Day	:";
		cin>>day;
		cout<<"Enter Month	:";
		cin>>month;
		cout<<"Enter Year	:";
		cin>>year;
	}
	void printRecord()
	{
		cout<<"Date:- "<<day<<":"<<month<<":"<<year<<":"<<endl;
	}
	void addNumberOfDays(int count)
	{
		int arr[]={31,28,31,30,31,30,31,31,30,31,30,31};
		int arr1[]={31,29,31,30,31,30,31,31,30,31,30,31};
		for(int i=0; i<count; i++)
		{
			if(((year%4==0 || year%400==0) && year%100!=0)!=0)
			{
				day++;
				if(day>arr1[month-1])
				{
					day=1;
					month=month+1;
					if(month==13)
					{
						month=1;
						year=year+1;
					}
				}
			}
			else
			{
				day++;
				if(day>arr[month-1])
				{
					day=1;
					month=month+1;
					if(month==13)
					{
						month=1;
						year=year+1;
					}
				}
			}
		}
	}


	string dayOfWeek()
	{
		acceptRecord();
		string str[7] = { "sunday","monday","tuesday","wednesday","thrusday","friday","saturday" };
		int cc, yc, mc, tempyear = 0, tempyc, lyear,result,a;
		int mccode[13] = { 0, 0, 3, 3, 6, 1, 4, 6, 2 ,5, 0, 3, 5 };
		int cccode[4] = { 6,4,2,0 };
		if ((year % 400 == 0 || year % 4 == 0) && year % 100 != 0)
		{
			a=1;
		}
		else
		{
			a=0;
		}
		lyear =a;
		if (lyear == 1)
		{
			if (month>0&&month<3)
			{
				mc = mccode[month] - 1;
			}
			else
			{
				mc = mccode[month];
			}
		}
		else if (lyear == 0)
		{
			mc = mccode[month];
		}
		if (year>=100&&year<=900)
		{
			int x;
			tempyc = year;
			tempyear = (year / 10) % 4;
			x = tempyc % 10;
			tempyc = tempyc / 10;
			x = (tempyc % 10) * 10 + x;
			yc = x / 4;
			cc = cccode[tempyear];
			result = (mc + cc + yc + day + x) % 7;
			return str[result];
		}
		else if (year>=1000&&year<=9999)
		{
			int x,y;
			tempyear = year;
			tempyc = year;
			x = tempyc % 10;
			tempyc = tempyc / 10;
			x = (tempyc % 10) * 10 + x;
			yc = x / 4;
			y = tempyear / 10;
			y = y / 10;
			cc = cccode[y%4];
			result = (mc + cc + yc + day + x) % 7;
			return str[result];
		}
		return 0;
	}

};
int main() {
	Date D;
	string a;
	int count;
	D.acceptRecord();
	D.printRecord();
	cout<<"enter number"<<endl;
	cin>>count;
	D.addNumberOfDays(count);
	D.printRecord();
	a=D.dayOfWeek();
	cout<<a;
	return 0;
}
