//============================================================================
// Name        : 4.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#define MAX 20
class Stack
{
private:
    int top;
    int a[MAX]; // Maximum size of Stack
public:
    Stack()
    {
    	top = -1;
    }
    void push(int x)
    {
        top++;
        a[top]=x;
    }
    int peek()
    {
       return a[top];
    }
    void isEmpty()
    {
       if(top<0){
    	   cout<<"stack is empty";
       }
    }
    int pop()
	{
    	int b;
    	b=a[top];
    	top--;
    	return b;
    }
};

int main(void)
{
	Stack s1;
	int a,b;
	s1.push(50);
	a=s1.peek();
	cout<<a<<endl;
	s1.isEmpty();
	b=s1.pop();
	cout<<b;
	s1.isEmpty();
}






