/*
 * oil.h
 *
 *  Created on: 18-Aug-2020
 *      Author: sunbeam
 */

#ifndef OIL_H_
#define OIL_H_
#include <iostream>
#include <string>
#include "service.h"
using namespace std;
class Oil : public Service
{
private:
	double cost;
public:
	Oil(void) :cost( 0.0){}
	Oil(const string& desc,const double cost);
	void setCost(const double cost);
	double getCost();
	virtual void acceptRecord();
	virtual void displayRecord();
	virtual double price();
	virtual ~Oil(){}
};

#endif /* OIL_H_ */
