#include "../include/vehicle.h"
Vehicle::Vehicle(const string& company,const string& model,const string& lnumber,const string& mobile)
{
	setCompany(company);
	setModel(model);
	setLNumber(lnumber);
	setMobile(mobile);
}
void Vehicle::setCompany(const string& company)
{
	for(unsigned i=0; i<company.length(); i++)
	{
		if(isdigit(company[i]))
			throw("Company Name Should be in alphabets");
	}
	this->company = company;
}
void Vehicle::setModel(const string& model)
{
	this->model = model;
}
void Vehicle::setLNumber(const string& lnumber)
{
	for(unsigned i=0; i<lnumber.length(); i++)
	{
		if(isalpha(lnumber[i]))
			throw("Vehicle number Should be in numeric");
	}
	this->lnumber = lnumber;
}
void Vehicle::setMobile(const string& mobile)
{
	this->mobile = mobile;
}
string& Vehicle::getCompany(void)
{
	return this->company;
}
string& Vehicle::getModel(void)
{
	return this->model;
}
string& Vehicle::getLNumber(void)
{
	return this->lnumber;
}
string& Vehicle::getMoblie(void)
{
	return this->mobile;
}
void Vehicle::acceptRecord()
{
	string comp;
	string model;
	string lnumber;
	cout<<"Enter Vehicle Company name	:	";
	cin>>comp;
	setCompany(comp);
	cout<<"Enter Vehicle Model name	:	";
	cin>>model;
	setModel(model);
	cout<<"Enter Vehicle Number		:	";
	cin>>lnumber;
	setLNumber(lnumber);
}
void Vehicle::display()
{
	cout<<company<<endl;
	cout<<model<<endl;
	cout<<lnumber<<endl;
}
ostream& operator<<(ostream &cout, const Vehicle& other)
{
	cout<<other.company<<","<<other.model<<", "<<other.lnumber<<", "<<other.mobile<<endl;
	return cout;
}
istream& operator>>(istream &cin, Vehicle& other)
{
	cout<<"Company	:	";
	cin>>other.company;
	for(unsigned i=0; i<other.company.length(); i++)
	{
		if(isdigit(other.company[i]))
			throw("Company Name Should be in alphabets");
	}
	cout<<"Model	:	";
	cin>>other.model;
	cout<<"LNumber	:	";
	cin>>other.lnumber;
	for(unsigned i=0; i<other.lnumber.length(); i++)
	{
		if(isalpha(other.lnumber[i]))
			throw("Vehicle number Should be in numeric");
	}
	return cin;
}
Vehicle Vehicle::operator=(const Vehicle &other)
{

	this->company = other.company;
	this->lnumber = other.lnumber;
	this->mobile  = other.mobile;
	this->model   = other.model;
	return *this;
}
