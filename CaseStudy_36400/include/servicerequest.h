/*
 * servicerequest.h
 *
 *  Created on: 19-Aug-2020
 *      Author: sunbeam
 */

#ifndef SERVICEREQUEST_H_
#define SERVICEREQUEST_H_
#include <iostream>
#include <string>
#include <list>
#include "service.h"
#include "maintenance.h"
#include "oil.h"
using namespace std;
class ServiceRequest
{
private:
	string customerName;
	string vehicleNumber;
	list<Service*> serviceList;
public:
	ServiceRequest(const string& customerName,const string& vehicleNumber);
	void setCustomerName(const string& customerName);
	void setVehicleNumber(const string& vehicleNumber);
	string& getCustomerName(void);
	string& getVehicleNumber(void);
	list<Service*>& getServiceList();
    void serviceProcess();
};

#endif /* SERVICEREQUEST_H_ */
