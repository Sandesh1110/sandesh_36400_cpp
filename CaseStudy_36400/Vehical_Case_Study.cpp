//============================================================================
// Name        : Vehical_Case_Study.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include "../include/servicestation.h"
using namespace std;

int mainMenu()
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Register New Customer."<<endl;
	cout<<"2.Servicing Request."<<endl;
	cout<<"3.Total Business."<<endl;
	cout<<"Enter Choice."<<endl;
	cin>>choice;
	return choice;
}
int main() {
	ServiceStation &s1 = ServiceStation::getInstance( "Morris_Garages" );
	Customer c;
	int choice;
	s1.loadCustomer();
	s1.loadBill();
	cout<<"||======================================================================||"<<endl;
	cout<<"||	    	Welcome To "<<s1.getName()<<"				||"<<endl;
	cout<<"||======================================================================||"<<endl;
	while((choice = mainMenu())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				cin>>c;
				s1.newCustomer(c);
				s1.storeCustomer();
				break;
			case 2:
				cout<<"||======================================================================||"<<endl;
				cout<<"||				"<<s1.getName()<<"				||";
				s1.displayCustomerList();
				s1.handleServiceRequest();
				cout<<"||======================================================================||"<<endl;
				s1.storeBill();
				break;
			case 3:
				cout<<"||======================================================================||"<<endl;
				cout<<"||				"<<s1.getName()<<"				||"<<endl;
				cout<<"||======================================================================||"<<endl;
				s1.totalCollection();
				cout<<"||======================================================================||"<<endl;
				break;
			}
		}
		catch(char const*a)
		{
			cout<<a<<endl;
		}
	}
	return 0;
}




