
#include "../include/maintenance.h"
Maintenance::Maintenance(const double laborCharges)
{
	setLaborCharges(laborCharges);
}
void Maintenance::setLaborCharges(const double laborCharges)
{
	this->laborCharges = laborCharges;
}
double Maintenance::getLaborCharges()
{
	return this->laborCharges;
}
void Maintenance::addPart()
{
	Part p;
	p.acceptRecord();
	partList.push_back(p);
}
list<Part>& Maintenance::getPartList()
{
	return partList;
}
void Maintenance::acceptRecord()
{
	Service::acceptRecord();
	double laborCharges;
	cout<<"Enter LaborCharges	:	";
	cin>>laborCharges;
	setLaborCharges(laborCharges);
}
void Maintenance::displayRecord()
{
	Service::displayRecord();
	cout<<"  Labor Charges"<<this->getLaborCharges()<<endl;
	list<Part> :: iterator p;
	for(p= partList.begin(); p != partList.end(); ++p)
	{
		p->displayRecord();
	}
}
double Maintenance::price()
{
	double total=this->laborCharges;
	list<Part> :: iterator p;
	for(p= partList.begin(); p != partList.end(); ++p)
	{
		total= total + p->getRate();
	}
	return total;
}

