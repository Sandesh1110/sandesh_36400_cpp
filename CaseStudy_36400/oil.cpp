#include "../include/oil.h"
Oil::Oil(const string& desc,const double cost)
	{
		this->setDesc(desc);
		setCost(cost);
	}
	void Oil::setCost(const double cost)
	{
		this->cost = cost;
	}
	double Oil::getCost()
	{
		return this->cost;
	}
	void Oil::acceptRecord()
	{
		Service::acceptRecord();
		double cost;
		cout<<"Enter Cost	:	";
		cin>>cost;
		setCost(cost);
	}
	 void Oil::displayRecord()
	{
		Service::displayRecord();
		cout<<"	Cost:  "<<this->getCost()<<endl;
	}
	 double Oil::price()
	{
		return this->cost;
	}

