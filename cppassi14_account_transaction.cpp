//============================================================================
// Name        : cppassi14_account_transaction.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<new>
using namespace std;
int count=1000;

class Transaction
{
private:
	int accNo;
	string date;
	string type;
	float amt;
public:
	Transaction()
{
		this->accNo =0;
		this->date = "05/08/2020";
		this->type = "zzzz";
		this->amt=0;
}
	void setAccNo(const int &accno)
	{
		this->accNo = accno;
	}
	void setDate(const string &date)
	{
		this->date = date;
	}
	void setType(const string &type)
	{
		this->type = type;
	}
	void setAmt(const float &amt)
	{
		this->amt = amt;
	}
	int getAccNo()const
	{
		return this->accNo;
	}
	string getDate()const
	{
		return this->date;
	}
	string getType()const
	{
		return this->type;
	}
	float getAmt()const
	{
		return this->amt;
	}
};

class Account
{
private:
	int accid;
	string name;
	string type;
	float balance;
public:
	Account(int accid = 0, string name = "", string type = "", float balance = 0.0) throw() : accid (accid), name (name), type (type), balance (balance)
{}
	void setAccid(const int &accid)
	{
		this->accid = accid;
	}
	void setName(const string &name)
	{
		this->name = name;
	}
	void setType(const string &type)
	{
		this->type = type;
	}
	void setBalance(const float &balance)
	{
		this->balance = balance;
	}
	int getAccid()const
	{
		return this->accid;
	}
	string getName()const
	{
		return this->name;
	}
	string getType()const
	{
		return this->type;
	}
	float getBalance()const
	{
		return this->balance;
	}
};

class Bank
{
private:
	int index;
	int tindex;
	Account *arr;
	Transaction *tarr;
public:
	Bank()
{
		index=-1;
		tindex=-1;
		arr=NULL;
		tarr=NULL;
}
	Bank(const int lenarr,const int lentarr)
	{
		index=-1;
		tindex=-1;
		arr = new Account[lenarr];
		tarr = new Transaction[lentarr];
	}
	void createAccount()
	{
		++index;
		arr[index].setAccid(++count);
		string name;
		cout<<"Name		:";
		cin>>name;
		arr[index].setName(name);
		string type;
		cout<<"Type		:";
		cin>>type;
		arr[index].setType(type);
		float balance;
		cout<<"Balance		:";
		cin>>balance;
		arr[index].setBalance(balance);
	}
	void displayAccount(const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if(accid==(arr[i].getAccid()))
			{
				cout<<"Account number	:"<<arr[i].getAccid()<<endl;
				cout<<"Name				:"<<arr[i].getName()<<endl;
				cout<<"Type				:"<<arr[i].getType()<<endl;
				cout<<"Balance			:"<<arr[i].getBalance()<<endl;
			}

		}
	}
	void amountDeposit(const float &amount,const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if(accid==(arr[i].getAccid()))
			{
				++tindex;
				float am;
				am=arr[i].getBalance()+amount;
				arr[i].setBalance(am);
				tarr[tindex].setAccNo(accid);
				tarr[tindex].setAmt(amount);
				tarr[tindex].setType("Deposit");
			}
		}
	}
	void amountWithdraw(const float &amount, const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=index; i++)
		{
			if((arr[i].getBalance())<amount)
				throw "Insufficient Balance";
			if(accid==arr[i].getAccid())
			{
				++tindex;
				float am;
				am=arr[i].getBalance()-amount;
				arr[i].setBalance(am);
				tarr[tindex].setAccNo(accid);
				tarr[tindex].setAmt(amount);
				tarr[tindex].setType("Withdraw");
			}
		}
	}
	void viewTransactionsList(const int &accid)
	{
		if(accid<1000 || accid>count)
			throw "Account number is Not Valid";
		for(int i=0; i<=tindex; i++)
		{
			if(accid==tarr[i].getAccNo())
			{
				cout<<"AccNo    :"<<"Date    :"<<"Type    :"<<"Amount   :"<<endl;
				cout<<tarr[i].getAccNo()<<"      "<<tarr[i].getDate()<<"      "<<tarr[i].getType()<<"    "<<tarr[i].getAmt()<<endl;
			}
		}
	}
	~Bank()
	{
		delete[] arr;
	    arr = NULL;
	    delete[] tarr;
	    tarr = NULL;
	}
};

int menu()
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Create Account."<<endl;
	cout<<"2.Display Account."<<endl;
	cout<<"3.Deposit Amount."<<endl;
	cout<<"4.Withdraw Amount."<<endl;
	cout<<"5.View Account Transaction."<<endl;
	cout<<"Enter Choice."<<endl;
	cin>>choice;
	return choice;
}

int getAccountNumber()
{
	int accno;
	cout<<"Enter Account Number		:";
	cin>>accno;
	return accno;
}

int getAmount()
{
	int amount;
	cout<<"Enter Amount		:";
	cin>>amount;
	return amount;
}

int main() {
	Bank b(5,20);
	int acc,amount,choice;
	while((choice=menu())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				b.createAccount();
				break;
			case 2:
				acc=getAccountNumber();
				b.displayAccount(acc);
				break;
			case 3:
				acc=getAccountNumber();
				amount=getAmount();
				b.amountDeposit(amount,acc);
				break;
			case 4:
				acc=getAccountNumber();
				amount=getAmount();
				b.amountWithdraw(amount,acc);
				break;
			case 5:
				acc=getAccountNumber();
				b.viewTransactionsList(acc);
			}
		}
		catch(char const* messege)
		{
			cout<<messege<<endl;
		}
		catch(...)
		{
			cout<<"terminated with exception value"<<endl;
		}
	}
	return 0;
}
