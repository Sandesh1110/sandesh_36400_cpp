//============================================================================
// Name        : 5.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Point
{
	int xPosition;
	int yPosition;
public:
	Point( void ) : xPosition (0), yPosition(0)
{
}
	Point(const int xPosition, const int yPosition )
	{
		this->xPosition=xPosition;
		this->yPosition=yPosition;
	}
	int getXPosition( void ) const
	{
		return this->xPosition;
	}
	void setXPosition(const int xPosition)
	{
		this->xPosition=xPosition;
	}
	int getYPosition( void ) const
	{
		return this->yPosition;
	}
	void setYPosition(const int yPosition)
	{
		this->yPosition=yPosition;
	}
};

void Accept(Point *ptr)
{
	int XPosition, YPosition;
	cout<<"Enter XPosition		:"<<endl;
	cin>>XPosition;
	ptr->setXPosition(XPosition);
	cout<<"Enter YPosition		:"<<endl;
	cin>>YPosition;
	ptr->setYPosition(YPosition);
}

void Display(Point *ptr)
{
	int XPosition, YPosition;
	XPosition=ptr->getXPosition();
	cout<<"XPosition		:"<<XPosition<<endl;
	YPosition=ptr->getYPosition();
	cout<<"YPosition		:"<<YPosition<<endl;
}

void Modify(Point *ptr)
{
	int XPosition, YPosition;
	cout<<"Enter XPosition		:"<<endl;
	cin>>XPosition;
	ptr->setXPosition(XPosition);
	cout<<"Enter YPosition		:"<<endl;
	cin>>YPosition;
	ptr->setYPosition(YPosition);
}

int menu_list()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept Values"<<endl;
	cout<<"2.Display Values"<<endl;
	cout<<"3.Modify Values"<<endl;
	cout<<"Enter Choice"<<endl;
	cin>>choice;
	return choice;
}

int main() {
	Point p;
	int choice;
	while((choice=menu_list())!=0)
	{
		switch(choice)
		{
		case 1:
			::Accept(&p);
			break;
		case 2:
			::Display(&p);
			break;
		case 3:
			Modify(&p);
			break;
		}
	}
	return 0;
}









