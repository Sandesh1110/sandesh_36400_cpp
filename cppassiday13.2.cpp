//============================================================================
// Name        : 2.cpp
// Author      : sandesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#define SIZE 5
using namespace std;
class Queue
{
private:
	int arr[SIZE];
	int front;
	int rear;
public:
	Queue(void)
{
		this->front=0;
		this->rear=0;
}
	bool is_queue_full(void)
	{
		if(rear == SIZE - 1)
			return true;
		else
			return false;
	}
	bool is_queue_empty(void)
	{
		if(front < 0 || front > rear)
			return true;
		else
			return false;
	}
	void enqueue(int ele)
	{
		if(is_queue_full())
			cout<<"Queue is Full"<<endl;
		else
		{
			rear = rear + 1;
			arr[rear] = ele;
		}
	}
	void dequeue(void)
	{
		if(is_queue_empty())
			cout<<"Queue is Empty"<<endl;
		else
		{
			front = front + 1;
		}
	}
	int get_front(void)
	{
		return arr[front];
	}
};

int menu()
{
	int choice;
	cout<<"0.Exit."<<endl;
	cout<<"1.Check Queue Is Full or not."<<endl;
	cout<<"2.Check Queue Is Empty or not."<<endl;
	cout<<"3.Enqueue."<<endl;
	cout<<"4.Dequeue."<<endl;
	cout<<"5.Get Front."<<endl;
	cout<<"Enter Choice."<<endl;
	cin>>choice;
	return choice;
}

int main()
{
	Queue q;
	int choice,b;
	bool a;
	while((choice=menu())!=0)
	{
		switch(choice)
		{
		case 1:
			a=q.is_queue_full();
			cout<<a<<endl;
			break;
		case 2:
			a=q.is_queue_empty();
			cout<<a<<endl;
			break;
		case 3:
			cout<<"Enter element	:";
			cin>>b;
			q.enqueue(b);
			break;
		case 4:
			q.dequeue();
			break;
		case 5:
			b=q.get_front();
			cout<<"front is 	:"<<b<<endl;
			break;
		}
	}
	return 0;
}





